//
//  NYCDataCom.swift
//  20180318-EdgarCedenoMunoz-NYCSchools
//
//  Created by Edgar Cedeño-Muñoz on 3/19/18.
//  Copyright © 2018 Edgar Cedeño-Muñoz. All rights reserved.
//

import Foundation
import JavaScriptCore

class NYCDataCom
{
  static let dataCom = NYCDataCom()
  private init() {}
  
  func collectListOfSchools(finished: @escaping (_ schoolList: [School]) -> Void)
  {
    guard let url = URL(string: "https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2")
    else
    {
      return
    }
    
    var request = URLRequest(url: url)
    request.httpMethod = "GET"
    request.timeoutInterval = 60
    
    let session = URLSession.shared
    
    let dataTask = session.dataTask(with: request,
                       completionHandler:
                        {
                          (data, response, error) in
                          if let error = error
                          {
                            print(error)
                          }
                          else
                          {
                            if let _ = data
                            {
                              // Get school name
                              //print("Data as String = \(String(describing: String(data: usableData, encoding: .utf8)))")
                              /*do
                              {
                                let jsonData = try JSONSerialization.jsonObject(with: usableData, options: []) as! [String: Any]
                                var schoolList = [School]()
                                for school in jsonData
                                {
                                  schoolList.append(School(dictionary: [school.key: school.value]))
                                }
                                finished(schoolList)
                              }
                              catch (let error)
                              {
                                print(error)
                              }*/
                            }
                            else
                            {
                              print("usable data is nil")
                            }
                          }
                        })
    dataTask.resume()
  }
  
  func getSATScoresUsing(_ school: School)
  {
    guard let url = URL(string: "https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4")
    else
    {
      return
    }
    
    var request = URLRequest(url: url)
    request.httpMethod = "GET"
    request.timeoutInterval = 60
    
    let session = URLSession.shared
    
    let dataTask = session.dataTask(with: request,
                                    completionHandler:
                                      {
                                        (data, response, error) in
                                        if let error = error
                                        {
                                          print(error)
                                        }
                                        else
                                        {
                                          if let _ = data
                                          {
                                            // Get SAT Scores
                                            //print("Data as String = \(String(describing: String(data: usableData, encoding: .utf8)))")
                                            /*do
                                             {
                                               let jsonData = try JSONSerialization.jsonObject(with: usableData, options: []) as! [String: Any]

                                               for score in jsonData
                                               {
                                                 if school["SchoolName"] == school.schoolName
                                                 {
                                                   school.score.mathScore = score["MathScore"]
                                                   school.score.readingScore = score["ReadingScore"]
                                                   school.score.writingScore = score["Writing"]
                                                 }
                                               }
                                             
                                             }
                                             catch (let error)
                                             {
                                               print(error)
                                             }*/
                                          }
                                          else
                                          {
                                            print("usable data is nil")
                                          }
                                        }
                                      })
    
    dataTask.resume()
  }
}
