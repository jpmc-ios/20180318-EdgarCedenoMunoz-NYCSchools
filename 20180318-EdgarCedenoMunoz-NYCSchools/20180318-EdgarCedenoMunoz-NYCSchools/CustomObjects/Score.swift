//
//  Score.swift
//  20180318-EdgarCedenoMunoz-NYCSchools
//
//  Created by Edgar Cedeño-Muñoz on 3/20/18.
//  Copyright © 2018 Edgar Cedeño-Muñoz. All rights reserved.
//

import Foundation

struct Score
{
  var mathScore: Int?
  var readingScore: Int?
  var writingScore: Int?
  init(mathScore: Int? = nil, readingScore: Int? = nil, writingScore: Int? = nil)
  {
    self.mathScore = mathScore
    self.readingScore = readingScore
    self.writingScore = writingScore
  }
}
