//
//  School.swift
//  20180318-EdgarCedenoMunoz-NYCSchools
//
//  Created by Edgar Cedeño-Muñoz on 3/19/18.
//  Copyright © 2018 Edgar Cedeño-Muñoz. All rights reserved.
//

import Foundation

struct School
{
  let schoolName: String?
  //var score: Score
  init(schoolName: String? = nil)
  {
    self.schoolName = schoolName
  }
  
  init(dictionary: [String: Any])
  {
    self.schoolName = dictionary["SchoolName"] as? String
  }
}
